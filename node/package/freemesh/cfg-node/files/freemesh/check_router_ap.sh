#!/bin/sh

manual_leds() {
	#kill the trigger so we control the leds
	echo "none" > /sys/class/leds/mt76-phy0/trigger;
	echo "none" > /sys/class/leds/zbt-we826\:green\:wifi/trigger;
}

if [ -f /tmp/normal_boot ] ; then 
	pidofflash=`ps | grep -i -E 'flashled' | grep -v grep`;
	kill -9 $pidofflash;

	manual_leds

	gateway_ip=`uci get fm.node.gateway_ip`;

	ping -c 1 $gateway_ip
	rc=$?

	if [[ $rc -eq 0 ]] ; then
		#connected
		#Turn on wifi LEDs
		echo 1 > /sys/class/leds/mt76-phy0/brightness;
		echo 1 > /sys/class/leds/zbt-we826\:green\:wifi/brightness;
	else
		#not connected
		#Alternate LEDs
		/freemesh/flashled.sh 1 &
	fi
fi
#else - do nothing since we are controlling the leds elsewhere
