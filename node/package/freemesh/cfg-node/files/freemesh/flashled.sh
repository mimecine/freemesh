#!/bin/sh

if  [ "$1" == 0 ]; then
	mt760=0;
	mt761=0;
elif [ "$1" == 1 ]; then
        mt760=0;
        mt761=1;
fi

while true
do
	if [ "$mt760" == 0 ]; then
		mt760=1;
	else
		mt760=0;
	fi 

	if [ "$mt761" == 0 ]; then 
        	mt761=1;
	else 
        	mt761=0;
	fi 

	echo $mt760 > /sys/class/leds/mt76-phy0/brightness;
	echo $mt761 > /sys/class/leds/zbt-we826\:green\:wifi/brightness;

	sleep 1;
done

